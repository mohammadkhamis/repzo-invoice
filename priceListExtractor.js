const errors = require("@feathersjs/errors");
module.exports = function (item, priceLists) {
  var id = item.variant.variant_id.toString();
  var listed_price = item.variant.listed_price;
  if (!Array.isArray(priceLists))
    throw new errors.Conflict(
      "priceLists argument passed to the SDK must be an array",
    );
  var foundPrice;
  if (Array.isArray(priceLists[0])) {
    foundPrice = searchLists(id, priceLists);
  } else {
    foundPrice = searchList(id, priceLists);
  }
  if (foundPrice != null) {
    return foundPrice;
  } else {
    return listed_price;
  }
};

function searchList(id, list) {
  var foundPrice = null;
  for (var i = 0; i < list.length; i++) {
    if (list[i].variant_id.toString() == id && foundPrice == null) {
      foundPrice = list[i].price;
    } else if (list[i].variant_id.toString() == id && foundPrice != null) {
      throw new errors.Conflict(
        "multiple prices for same varaint ID is detected",
      );
    }
  }
  return foundPrice;
}

function searchLists(id, lists) {
  var foundPrice = null;
  for (var i = 0; i < lists.length; i++) {
    for (var j = 0; j < lists[i].length; j++) {
      if (lists[i][j].variant_id.toString() == id && foundPrice == null) {
        foundPrice = lists[i][j].price;
      } else if (
        lists[i][j].variant_id.toString() == id &&
        foundPrice != null
      ) {
        throw new errors.Conflict(
          "multiple prices for same varaint ID is detected",
        );
      }
    }
  }
  return foundPrice;
}
