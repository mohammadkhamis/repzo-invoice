module.exports = INVOICE;
const errors = require("@feathersjs/errors");
const priceListExtractor = require("./priceListExtractor");

/**
  @param {Object} HeaderData
  @param {String} client_id 
  @param {String} client_name
  @param {Object} item 
*/
//console.log("SDK Started");

/**
<        "_id": "5b9f9d16d0e035780ace380d",
        "name": "hasan alawneh",
        "type": "rep"
    }>
  version: "Integer",
  latest: "Bolean",
  issue_date: "String",
  due_date :"String",
  serial_number : "String",
  origin_warehouse : "ID",
  company_namespace :"namespace" ,
  fullinvoice_id :"ID"}
   */
function INVOICE(HeaderData, promotions, priceLists, engineOptions) {
  this.client_id = HeaderData.client_id;
  if (HeaderData.payment_type) this.payment_type = HeaderData.payment_type;
  if (HeaderData.comment) this.comment = HeaderData.comment;
  this.client_name = HeaderData.client_name;
  this.creator = HeaderData.creator;
  this.version = HeaderData.version;
  this.latest = HeaderData.latest;
  this.sync_id = HeaderData.sync_id;
  this.status = HeaderData.status;
  this.issue_date = HeaderData.issue_date;
  this.due_date = HeaderData.due_date;
  this.serial_number = HeaderData.serial_number;
  this.origin_warehouse = HeaderData.origin_warehouse;
  this.msl_sales = HeaderData.msl_sales;
  this.company_namespace = HeaderData.company_namespace;
  this.fullinvoice_id = HeaderData.fullinvoice_id;
  this.currency = HeaderData.currency;
  this.type = HeaderData.type;
  this.processable = HeaderData.processable;
  this.failure_reasons = HeaderData.failure_reasons;
  this.overwriteDeductionFixed = HeaderData.overwriteDeductionFixed;
  this.overwriteDeductionRatio = HeaderData.overwriteDeductionRatio;
  this.deductionFixed =
    HeaderData.overwriteDeductionFixed ||
    HeaderData.overwriteDeductionFixed == 0
      ? HeaderData.overwriteDeductionFixed
      : HeaderData.promoDeductionFixed
      ? HeaderData.promoDeductionFixed
      : 0;
  this.deductionRatio =
    HeaderData.overwriteDeductionRatio ||
    HeaderData.overwriteDeductionRatio == 0
      ? HeaderData.overwriteDeductionRatio
      : HeaderData.promoDeductionRatio
      ? HeaderData.promoDeductionRatio
      : 0;
  this.tax_exempt = HeaderData.tax_exempt;
  this.overwriteTaxExempt = HeaderData.overwriteTaxExempt;
  this.shipping_fixed_price = HeaderData.shipping_fixed_price;
  this.shipping_discount_amount = HeaderData.shipping_discount_amount;
  this.shipping_discount_ratio = HeaderData.shipping_discount_ratio;
  HeaderData._id && (this._id = HeaderData._id);

  this.promotions = promotions;
  this.priceLists = priceLists;
  this.return_serial_number = HeaderData.return_serial_number;
  this.notes = {};
  this.taxes = {};
  this.external_serial_number = HeaderData.external_serial_number;
  this.qr_code_tlv = HeaderData.qr_code_tlv;
  this.workorder = HeaderData.workorder;
  this.asset = HeaderData.asset;
  this.asset_unit = HeaderData.asset_unit;
  this.signature = HeaderData.signature;

  // Net
  this.subtotal = 0;
  this.discount_amount = 0;
  this.taxable_subtotal = 0;
  this.net_total = 0;
  this.tax_amount = 0;
  this.total = 0;

  this.items = [];
  this.return_items = [];

  // Pre
  this.pre_subtotal = 0;
  this.pre_discount_amount = 0;
  this.pre_taxable_subtotal = 0;
  this.pre_tax_amount = 0;
  this.pre_total = 0;
  this.pre_net_total = 0;

  // Return
  this.return_subtotal = 0;
  this.return_discount_amount = 0;
  this.return_taxable_subtotal = 0;
  this.return_tax_amount = 0;
  this.return_total = 0;
  this.return_net_total = 0;

  // Shipping
  this.shipping_zone = HeaderData.shipping_zone;
  this.payment_method = HeaderData.payment_method;
  this.shipping_price = 0;
  this.shipping_tax = 0;
  this.shipping_charge = 0;
  this.payment_charge = 0;
  this.total_with_charges = 0;

  this.calculate = (item, options) => {
    try {
      item.base_unit_qty = item.qty * item.measureunit.factor;
      if (
        item.overwritePrice != undefined &&
        item.overwritePrice !== "" &&
        item.overwritePrice != null
      ) {
        //item.price = Math.round(item.overwritePrice);
        item.discounted_price = Math.round(item.overwritePrice);
      } else {
        item.price = priceListExtractor(item, this.priceLists); //||
        // item.variant.listed_price;
        item.discounted_price = options.roundDiscountedPriceAtPromo
          ? Math.round(getDiscountedPrice(item))
          : getDiscountedPrice(item);
      }
      if (
        options.isReturn == true &&
        (item.hidden_price || item.hidden_price == 0)
      ) {
        item.discounted_price = item.hidden_price;
      }
      item.discount_value = item.price - item.discounted_price;
      item.taxable_subtotal = Math.round(
        item.discounted_price * item.base_unit_qty,
      );
      if (item.discount_value < 0) item.discount_value = 0;
      this.calculateTheModifiers(item);

      let tax_amount, gross_value;
      if (item.tax.type == "additive" || item.tax.type == "N/A") {
        tax_amount = this.tax_exempt
          ? 0
          : item.discounted_price * item.tax.rate;
        gross_value = tax_amount + item.discounted_price;
      } else if (item.tax.type == "inclusive") {
        tax_amount = this.tax_exempt
          ? 0
          : item.discounted_price - item.discounted_price / (item.tax.rate + 1);
        gross_value = item.discounted_price;
      } else {
        throw new errors.BadRequest(
          "Tax type of " + item.tax.type + " is not accepted",
        );
      }
      item.tax_amount = Math.round(tax_amount);
      item.gross_value = Math.round(gross_value);
      const tax_total_without_modifiers = tax_amount * item.base_unit_qty;
      item.tax_total_without_modifiers = Math.round(
        tax_total_without_modifiers,
      );
      const line_total_without_modifiers = item.base_unit_qty * gross_value;
      item.line_total_without_modifiers = Math.round(
        line_total_without_modifiers,
      );
      item.total_before_tax_without_modifiers = Math.round(
        line_total_without_modifiers - tax_total_without_modifiers,
      );
      item.line_total = Math.round(
        item.base_unit_qty * (item.modifiers_total + gross_value),
      );
      item.total_before_tax = Math.round(
        item.base_unit_qty *
          (item.modifiers_total_before_tax + gross_value - tax_amount),
      );
      item.tax_total = Math.round(
        item.base_unit_qty * (item.modifiers_tax_total + tax_amount),
      );

      item.modifiers_total = Math.round(item.modifiers_total);
      item.modifiers_total_before_tax = Math.round(
        item.modifiers_total_before_tax,
      );
      item.modifiers_tax_total = Math.round(item.modifiers_tax_total);
      // this.calculateItemDeduction(item);

      item.discounted_price = Math.round(item.discounted_price);
      item.discount_value = Math.round(item.discount_value);
      return item;
    } catch (e) {
      throw e;
    }
  };

  this.calculateItemDeduction = (item) => {
    item.deductionRatio_r =
      this.total && (this.deductionRatio || this.deductionRatio == 0)
        ? this.deductionRatio
        : 0;
    item.deductionRatio_f =
      this.total && this.deductionFixed
        ? Math.min(this.deductionFixed / this.total, 1)
        : 0;
    item.deductionRatio = Math.min(
      1,
      item.deductionRatio_f + item.deductionRatio_r,
    );
    delete item.deductionRatio_f, item.deductionRatio_r;
    item.deductedTax = item.deductionRatio
      ? Math.round(item.tax_total * item.deductionRatio)
      : 0;
    item.deduction = item.deductionRatio
      ? Math.round(item.line_total * item.deductionRatio)
      : 0;
    item.deductionBeforeTax = Math.round(item.deduction - item.deductedTax);
    item.lineTotalAfterDeduction = Math.round(item.line_total - item.deduction);
  };

  this.calculateTheModifiers = (item) => {
    try {
      item.modifiers_total = 0;
      item.modifiers_total_before_tax = 0;
      item.modifiers_tax_total = 0;
      if (item.modifiers_groups) {
        item.modifiers_groups.forEach((modifiers_group) => {
          modifiers_group.group_total = 0;
          modifiers_group.group_total_before_tax = 0;
          modifiers_group.group_tax_total = 0;

          if (modifiers_group.modifiers) {
            modifiers_group.modifiers.forEach((modifier) => {
              modifier.discounted_price =
                modifier.overwritePrice || modifier.price;

              let tax_total, line_total, total_before_tax;
              if (item.tax.type == "additive" || item.tax.type == "N/A") {
                const tax_amount = this.tax_exempt
                  ? 0
                  : modifier.discounted_price * item.tax.rate;
                const gross_value = tax_amount + modifier.discounted_price;
                modifier.tax_amount = Math.round(tax_amount);
                modifier.gross_value = Math.round(gross_value);

                tax_total = tax_amount;
                line_total = tax_amount + modifier.discounted_price;
                total_before_tax = modifier.discounted_price;
              } else if (item.tax.type == "inclusive") {
                const tax_amount = this.tax_exempt
                  ? 0
                  : modifier.discounted_price -
                    modifier.discounted_price / (item.tax.rate + 1);
                const gross_value = modifier.discounted_price;
                modifier.tax_amount = Math.round(tax_amount);
                modifier.gross_value = Math.round(gross_value);

                tax_total = tax_amount;
                line_total = gross_value;
                total_before_tax =
                  modifier.discounted_price / (item.tax.rate + 1);
              } else {
                throw new errors.BadRequest(
                  "Tax type of " + item.tax.type + " is not accepted",
                );
              }

              modifiers_group.group_tax_total += tax_total;
              modifiers_group.group_total += line_total;
              modifiers_group.group_total_before_tax += total_before_tax;
            });
          }
          item.modifiers_total += modifiers_group.group_total;
          item.modifiers_total_before_tax +=
            modifiers_group.group_total_before_tax;
          item.modifiers_tax_total += modifiers_group.group_tax_total;

          modifiers_group.group_total = Math.round(modifiers_group.group_total);
          modifiers_group.group_total_before_tax = Math.round(
            modifiers_group.group_total_before_tax,
          );
          modifiers_group.group_tax_total = Math.round(
            modifiers_group.group_tax_total,
          );
        });
      }
    } catch (e) {
      throw e;
    }
  };

  this.totals = (items, load) => {
    const result = {
      subtotal: 0,
      discount_amount: 0,
      taxable_subtotal: 0,
      net_total: 0,
      tax_amount: 0,
      total: 0,
      totalAfterDeduction: 0,
      totalDeductedTax: 0,
      totalDeduction: 0,
      totalDeductionBeforeTax: 0,
    };
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      result.tax_amount += item.tax_total;
      result.subtotal += Math.round(item.price * item.base_unit_qty);
      result.discount_amount += Math.round(
        item.discount_value * item.base_unit_qty,
      );
      result.taxable_subtotal += Math.round(
        item.discounted_price * item.base_unit_qty,
      );
      result.total += item.line_total;
      result.net_total = result.taxable_subtotal;
      if (load) {
        result.totalDeductedTax = result.totalDeductedTax
          ? result.totalDeductedTax + item.deductedTax
          : item.deductedTax;
        result.totalDeduction = result.totalDeduction
          ? result.totalDeduction + item.deduction
          : item.deduction;
        result.totalDeductionBeforeTax = result.totalDeductionBeforeTax
          ? result.totalDeductionBeforeTax + item.deductionBeforeTax
          : item.deductionBeforeTax;
        result.totalAfterDeduction = result.totalAfterDeduction
          ? result.totalAfterDeduction + item.lineTotalAfterDeduction
          : item.lineTotalAfterDeduction;
        result.net_total =
          result.taxable_subtotal - result.totalDeductionBeforeTax;
      }
    }
    return result;
  };

  this.netTotal = () => {
    try {
      this.subtotal = this.pre_subtotal - this.return_subtotal;
      this.discount_amount =
        this.pre_discount_amount - this.return_discount_amount;
      this.taxable_subtotal =
        this.pre_taxable_subtotal - this.return_taxable_subtotal;
      this.net_total = this.pre_net_total - this.return_net_total;
      this.tax_amount = this.pre_tax_amount - this.return_tax_amount;
      this.total = this.pre_total - this.return_total;
      if (this.shipping_zone && this.shipping_zone.shipping_method) {
        this.shipping_price =
          this.shipping_fixed_price || this.shipping_fixed_price === 0
            ? this.shipping_fixed_price
            : this.shipping_discount_amount
            ? Math.max(
                this.shipping_zone.shipping_method.rate -
                  this.shipping_discount_amount,
                0,
              )
            : this.shipping_discount_ratio
            ? this.shipping_zone.shipping_method.rate -
              this.shipping_zone.shipping_method.rate *
                this.shipping_discount_ratio
            : this.shipping_zone.shipping_method.rate;
        const tax = this.shipping_zone.shipping_method.tax;
        if (tax.type == "additive" || tax.type == "N/A") {
          this.shipping_tax = Math.round(this.shipping_price * tax.rate);
          this.shipping_charge = Math.round(
            this.shipping_price * (1 + tax.rate),
          );
        } else if (tax.type == "inclusive") {
          this.shipping_tax = Math.round(
            this.shipping_price - this.shipping_price / (tax.rate + 1),
          );
          this.shipping_charge = Math.round(this.shipping_price);
        } else {
          throw new errors.BadRequest(
            "Tax type of " + tax.type + " is not accepted",
          );
        }
      }

      if (
        this.payment_method &&
        this.payment_method.fee &&
        this.payment_method.rate
      ) {
        this.payment_charge =
          this.total * this.payment_method.rate + this.payment_method.fee;
      } else {
        this.payment_charge = 0;
      }
      this.total_with_charges =
        this.total + this.payment_charge + this.shipping_charge;
    } catch (e) {
      throw e;
    }
  };

  this.addItem = (item, options) => {
    try {
      if (item.tax == undefined) {
        item.tax = { type: "N/A", rate: 0, name: "No Tax" };
      }

      if (
        options.isReturn == false &&
        (item.promotions == undefined || !options.old)
      ) {
        item.promotions = {
          isGet: false,
          taken: 0,
          free: item.base_unit_qty,
          bookings: [],
        };
      }

      item = this.calculate(item, options);
      if (options.isReturn == false && !options.old) {
        item.promotions.free = item.base_unit_qty;
      }
      return item;
    } catch (e) {
      throw e;
    }
  };

  this.load = (items, options) => {
    try {
      // items.forEach(item=>item.deductionRatio=0.05) // adding deduction for testing must be deleted
      // this.deductionRatio=0.05 // adding deduction for testing must be deleted
      // this.deductionFixed=5000    // adding deduction for testing must be deleted
      options = options || {
        old: false,
        isReturn: false,
        roundDiscountedPriceAtPromo:
          engineOptions?.roundDiscountedPriceAtPromo || false,
      };
      this.items.splice(0, this.items.length);
      this.subtotal = 0;
      this.discount_amount = 0;
      this.taxable_subtotal = 0;
      this.net_total = 0;
      this.tax_amount = 0;
      this.total = 0;
      this.pre_subtotal = 0;
      this.pre_discount_amount = 0;
      this.pre_taxable_subtotal = 0;
      this.net_total = 0;
      this.pre_tax_amount = 0;
      this.pre_total = 0;
      this.pre_net_total = 0;
      this.taxes = {};
      for (let i = 0; i < items.length; i++) {
        if (!items[i].tax || !items[i].tax.type) {
          items[i].tax = { type: "N/A", rate: 0, name: "No Tax" };
        }
        items[i].class = "invoice";
        if (!this.tax_exempt) {
          this.taxes[items[i].tax.name] = this.taxes[items[i].tax.name] || {
            name: items[i].tax.name,
            rate: items[i].tax.rate,
            total: 0,
            type: items[i].tax.type,
          };
          this.taxes[items[i].tax.name].total += items[i].tax_total;
        }
        this.items.push(this.addItem(items[i], options));
        items[i].dex = i;
      }
      this.netTotal();
      this.total = this.totals(this.items, true).total;
      this.items.forEach((item) => this.calculateItemDeduction(item));
      let pre_result = this.totals(this.items, true);
      this.totalAfterDeduction = pre_result.totalAfterDeduction;
      this.commulateTaxTotal(pre_result);
      this.totalDeductedTax = pre_result.totalDeductedTax;
      this.totalDeduction = pre_result.totalDeduction;
      this.totalDeductionBeforeTax = pre_result.totalDeductionBeforeTax;

      this.pre_subtotal = pre_result.subtotal;
      this.pre_discount_amount =
        pre_result.discount_amount + pre_result.totalDeduction;
      this.pre_taxable_subtotal = pre_result.taxable_subtotal;
      this.pre_net_total = pre_result.net_total;
      this.pre_tax_amount = pre_result.tax_amount - pre_result.totalDeductedTax;
      this.pre_total = pre_result.total - pre_result.totalDeduction;

      this.netTotal();
    } catch (e) {
      console.error(e);
      throw e;
    }
  };
  this.loadPriceLists = (priceLists) => {
    try {
      // items.forEach(item=>item.deductionRatio=0.05) // adding deduction for testing must be deleted
      // this.deductionRatio=0.05 // adding deduction for testing must be deleted
      // this.deductionFixed=5000    // adding deduction for testing must be deleted;
      this.priceLists = priceLists;
      let items = this.items || [];
      let return_items = this.return_items;
      this.load(items);
      this.return_load(return_items);
    } catch (e) {
      console.error(e);
      throw e;
    }
  };
  this.commulateTaxTotal = (pre_result) => {
    if (this.deductionRatio || this.deductionFixed) {
      Object.keys(this.taxes).forEach((taxName) => {
        let tax = this.taxes[taxName];
        if (tax.type == "additive" || tax.type == "inclusive") {
          if (this.deductionRatio) {
            tax.total -= Math.round(tax.total * this.deductionRatio);
          }
          if (this.deductionFixed) {
            tax.total -= Math.round(
              (this.deductionFixed * tax.total) / this.totalAfterDeduction,
            );
          }
        }
      });
    } else {
    }
  };
  this.forgeSerialNumber = (identifier, count, type) => {
    try {
      let prefix;
      if (type == "proforma") {
        prefix = "PRO";
      } else if (type == "invoice") {
        prefix = "INV";
      } else {
        throw new errors.BadRequest("Unknown type of invoices");
      }
      const formatted = prefix + "-" + identifier + "-" + count.toString();
      return {
        formatted: formatted,
        identifier: identifier,
        count: count,
      };
    } catch (e) {
      throw e;
    }
  };

  this.toJson = () => {
    try {
      var JSON = {
        client_id: this.client_id,
        client_name: this.client_name,
        creator: this.creator,
        version: this.version,
        latest: this.latest,
        status: this.status,
        issue_date: this.issue_date,
        due_date: this.due_date,
        serial_number: this.serial_number,
        return_serial_number: this.return_serial_number,
        origin_warehouse: this.origin_warehouse,
        msl_sales: this.msl_sales,
        company_namespace: this.company_namespace,
        fullinvoice_id: this.fullinvoice_id,
        subtotal: this.subtotal,
        discount_amount: this.discount_amount,
        tax_amount: this.tax_amount,
        total: this.total,
        promotions: this.promotions,
        priceLists: this.priceLists,
        taxable_subtotal: this.taxable_subtotal,
        net_total: this.net_total,
        items: this.items,
        return_items: this.return_items,
        notes: this.notes,
        pre_subtotal: this.pre_subtotal,
        pre_discount_amount: this.pre_discount_amount,
        pre_taxable_subtotal: this.pre_taxable_subtotal,
        pre_net_total: this.pre_net_total,
        pre_tax_amount: this.pre_tax_amount,
        pre_total: this.pre_total,
        return_subtotal: this.return_subtotal,
        return_discount_amount: this.return_discount_amount,
        return_taxable_subtotal: this.return_taxable_subtotal,
        return_net_total: this.return_net_total,
        return_tax_amount: this.return_tax_amount,
        return_total: this.return_total,
        totalDeductedTax: this.totalDeductedTax,
        totalDeduction: this.totalDeduction,
        overwriteDeductionFixed: this.overwriteDeductionFixed,
        overwriteDeductionRatio: this.overwriteDeductionRatio,
        totalDeductionBeforeTax: this.totalDeductionBeforeTax,
        deductionRatio: this.deductionRatio,
        deductionFixed: this.deductionFixed,
        _id: this._id,
        sync_id: this.sync_id,
        notes: this.notes,
        shipping_zone: this.shipping_zone,
        payment_method: this.payment_method,
        shipping_price: this.shipping_price,
        shipping_tax: this.shipping_tax,
        shipping_charge: this.shipping_charge,
        payment_charge: this.payment_charge,
        total_with_charges: this.total_with_charges,
        taxes: this.taxes,
        tax_exempt: this.tax_exempt,
        overwriteTaxExempt: this.overwriteTaxExempt,
        shipping_fixed_price: this.shipping_fixed_price,
        shipping_discount_amount: this.shipping_discount_amounts,
        shipping_discount_ratio: this.shipping_discount_ratio,
        currency: this.currency,
        type: this.type,
        processable: this.processable,
        failure_reasons: this.failure_reasons,
        external_serial_number: this.external_serial_number,
        qr_code_tlv: this.qr_code_tlv,
        workorder: this.workorder,
        asset: this.asset,
        asset_unit: this.asset_unit,
        signature: this.signature,
      };
      if (this.payment_type) JSON.payment_type = this.payment_type;
      if (this.comment) JSON.comment = this.comment;
      return JSON;
    } catch (e) {
      throw e;
    }
  };

  this.return_load = (return_items) => {
    try {
      this.return_items.splice(0, this.return_items.length);
      // Net
      this.subtotal = 0;
      this.discount_amount = 0;
      this.taxable_subtotal = 0;
      this.net_total = 0;
      this.tax_amount = 0;
      this.total = 0;
      // Return
      this.return_subtotal = 0;
      this.return_discount_amount = 0;
      this.return_taxable_subtotal = 0;
      this.return_net_total = 0;
      this.return_tax_amount = 0;
      this.return_total = 0;
      for (let i = 0; i < return_items.length; i++) {
        if (!return_items[i].tax || !return_items[i].tax.type) {
          return_items[i].tax = { type: "N/A", rate: 0, name: "No Tax" };
        }
        this.taxes[return_items[i].tax.name] = this.taxes[
          return_items[i].tax.name
        ] || {
          name: return_items[i].tax.name,
          rate: return_items[i].tax.rate,
          total: 0,
          type: return_items[i].tax.type,
        };
        this.taxes[return_items[i].tax.name].total -= return_items[i].tax_total;
        return_items[i].class = "return";
        delete return_items[i].promotions;
        this.return_items.push(
          this.addItem(return_items[i], { isReturn: true }),
        );
        return_items[i].dex = i;
      }
      this.return_items.forEach((return_item) => {
        if (typeof return_item.hidden_price != "number") {
          return_item.hidden_price = undefined;
        }
      });
      const return_result = this.totals(this.return_items);
      this.return_items.forEach((item) => this.calculateItemDeduction(item));
      this.return_subtotal = return_result.subtotal;
      this.return_discount_amount = return_result.discount_amount;
      this.return_taxable_subtotal = return_result.taxable_subtotal;
      this.return_net_total = return_result.net_total;
      this.return_tax_amount = return_result.tax_amount;
      this.return_total = return_result.total;
      this.netTotal();
    } catch (e) {
      console.error(e);
      throw e;
    }
  };
}

const getDiscountedPrice = (item) => {
  try {
    let res;
    if (
      item.promotions &&
      (item.promotions.promoPrice || item.promotions.promoPrice == 0)
    ) {
      if (
        item.promotions.lineDeductionRatio ||
        item.promotions.lineDeductionFixed ||
        item.promotions.lineFixedPrice ||
        item.promotions.lineFixedPrice == 0
      ) {
        let adptedPromoPrice = Math.max(
          0,
          item.promotions.promoPrice -
            item.promotions.promoPrice *
              (item.promotions.lineDeductionRatio
                ? item.promotions.lineDeductionRatio
                : 0) -
            (item.promotions.lineDeductionFixed
              ? item.promotions.lineDeductionFixed
              : 0),
        );
        res = Math.min(
          item.promotions.lineFixedPrice || item.promotions.lineFixedPrice == 0
            ? item.promotions.lineFixedPrice
            : adptedPromoPrice,
          adptedPromoPrice,
        );
      } else {
        res = item.promotions.promoPrice;
      }
    } else if (
      item.promotions &&
      (item.promotions.lineDeductionRatio ||
        item.promotions.lineDeductionFixed ||
        item.promotions.lineFixedPrice ||
        item.promotions.lineFixedPrice == 0)
    ) {
      let adptedPrice = Math.max(
        0,
        item.price -
          item.price *
            (item.promotions.lineDeductionRatio
              ? item.promotions.lineDeductionRatio
              : 0) -
          (item.promotions.lineDeductionFixed
            ? item.promotions.lineDeductionFixed
            : 0),
      );
      res = Math.min(
        item.promotions.lineFixedPrice || item.promotions.lineFixedPrice == 0
          ? item.promotions.lineFixedPrice
          : adptedPrice,
        adptedPrice,
      );
    } else {
      res = item.price;
    }

    return res;
  } catch (e) {
    console.error(e);
    throw e;
  }
};
